#!/bin/bash

cd /var/www/content/laravel6-voyager

. wait_for_mariadb.sh
. wait_for_mysql.sh

/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS 'joomla3'@'%' IDENTIFIED BY '123123123';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS joomla3 CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON joomla3.* TO 'joomla3'@'%';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'

/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS 'joomla3'@'%' IDENTIFIED BY '123123123';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS joomla3 CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON joomla3.* TO 'joomla3'@'%';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'
